---
layout: page
title: Über uns
permalink: /about/
lang: de
---

*The European Data Retention Atlas* ist ein Freizeit-Projekt einiger
Datenschutz- und Privacy-Aktivisti, momentan hauptsächlich aus Deutschland.

Es wurde losgetreten von [ulif](https://codeberg.org/ulif) um den Mangel an
aktuellen Infos zu den ganzen verschiedenen Vorratsdatenspeicherungen in der EU
zu begegnen. Wir tun unser Bestes, um aktuell zu bleiben und versuchen
Neuigkeiten aus dem Bereich so schnell wie möglich zu bekommen. Aber wir können
nicht garantieren, dass alle Informationen auf dieser Webpräsenz zu allen
Zeiten und komplett korrekt und vollständig sind.


## Hinweise und Fehler

Sollten sie Fehler finden oder über neue Infos zu Vorratsdatenspeicherungen in
der EU verfügen, würden wir uns über eine Kontaktaufnahme freuen. Sie können
die Kommunikationskanäle in der Seitenleiste nutzen. Oder, was noch besser ist,
sie benutzen unseren [Fehler
Tracker](https://codeberg.org/ulif/atlas-of-dataretention/issues) bei
`Codeberg`. Falls sie Neuigkeiten haben, wäre es erste Sahne, wenn sie
öffentlich verfügbare und seriöse Quellen hinzufügen könnten.

Wir finden es schön, sich mit anderen Aktivisti in Europa und der Welt zu
vernetzen, denn das macht es für alle besser. Melden sie sich bitte, wenn sie
finden, dass wir uns kennenlernen sollten.


## Privacy Policy


### IP logging

Diese Webpräsenz wird von [codeberg](https://codeberg.org) gehostet, einem sehr
guten Quellcode-Hoster aus Deutschland. Codeberg verfolgt eine
"Minimum-Collection Policy", was bedeutet, dass sie sowenig Daten wie möglich
sammeln. Wenn sie diese Webpräsenz besuchen, verbinden sie sich mit den Servern
von Codeberg. Die Server-Logdateien können ihre IP-Adresse und
"User-Agent"-Kennungen ihrer dabei benutzten Endgeräte enthalten. Diese
Logdateien werden nach Angaben von Codeberg automatisch nach maximal sieben
Tagen gelöscht.

Im Gegensatz zu so ziemlich allen EU-Mitgliedsstaaten tracken wir sie nicht.


### Cookies

Cookies sind relativ süße Kekse mit einer ziemlich weichen Textur. Sie
enthalten gewöhnlich Schoko- oder Fruchtstückchen und sind sehr lecker.

Wir nutzen Cookies um hungrige Aktivisti zu stärken, für den Kampf gegen
Vorratsdatenspeicherungen und die Überwachung von Menschen in der EU.

Im Gegensatz zur EU-Kommission erzählen wir ihnen deshalb auch nicht, dass
Cookies gut für ihr Browsing-Erlebnis sind.


## Impressum

Impressum nach §5 TMG

>   Stratum 0 e.V.<br>
>   c/o ulif / The European Data Retention Atlas<br>
>   Hamburger Str. 273A, Eingang A2<br>
>   D-38114 Braunschweig<br>
>   Germany
    

## Technischer Hintergrund

Technisch basiert diese Webpräsenz auf [jekyll](https://jekyllrb.com), mit
einer leicht modifizierten Variante des exquisiten [basically basic
theme](https://github.com/mmistakes/jekyll-theme-basically-basic) von [Micheal
Rose](https://mademistakes.com/). Das Hosting erfolgt gegenwärtig auf
[codeberg](https://codeberg.org), einer sehr empfehlenswerten Alternative zu
`github`.

*Disclaimer*: Die Entwicklung der belgischen Layer-2-Karte wurde 2022 mit 999 EUR von [Patrick
Breyer MEP][patrick-breyer] (Greens/EFA group, Pirate Party) unterstützt (war
auch richtig viel Arbeit).

[patrick-breyer]: https://www.europarl.europa.eu/meps/en/197431/PATRICK_BREYER/home
