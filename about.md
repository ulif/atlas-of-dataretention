---
layout: page
title: About
permalink: /about/
lang: en
---

The Atlas of Data Retention in the EU is a sparetime project of some data
protection and privacy activists, mainly from Germany.

It was started by [ulif](https://codeberg.org/ulif) to tackle the lack of
up-to-date infos about data retention all over the EU. We try hard to update
and examine our data as quick as possible but we cannot guarantee to be
accurate in all details all of the time.


## Want to help?

If you found an error or want to share relevant news about data retention in
the EU, we would love to hear from you. You can use the communication channels
in the side bar or, even better, file an issue on our codeberg [issue
tracker](https://codeberg.org/ulif/atlas-of-dataretention/issues). Ideally you
add available public sources of your news.

We love to connect to other privacy activists in Europe. Please drop us a line
if you think we should get known to each other.


## Privacy Policy


### IP logging

This site is hosted by [codeberg](https://codeberg.org), an awesome source
code-hoster from Germany. Codeberg is following a minimum-collection policy. If
you visit this site, you connect to servers of codeberg. The server log files
can contain client IP addresses and user agent strings from connecting
computers. These log files are destroyed automatically within at most seven
days.

Contrary to almost all EU member states, we do not track you.


### Cookies

Cookies are sweet biscuits with a fairly soft, chewy texture and typically
containing pieces of chocolate or fruit.

We use cookies to feed hungry activists and gain more energy for the fight
against data retentions and surveillance of citizens in the EU.




## Impressum / Imprint

Impressum nach §5 TMG (Imprint according to German Law)

An Impressum (Latin impressum, usually translated to "Imprint" in analogy to
the printer's imprint according to UK law) is the legally mandated statement of
the ownership and authorship of a document, which must be included in books,
newspapers, magazines and websites published in Germany and certain other
German-speaking countries, such as Austria and Switzerland. The
Telemediengesetz (TMG, "Telemedia Act") mandates an Impressum, ours you find
here:

>   Stratum 0 e.V.<br>
>   c/o ulif / The European Data Retention Atlas<br>
>   Hamburger Str. 273A, Eingang A2<br>
>   D-38114 Braunschweig<br>
>   Germany
    



## Technical Background

Technically, the site is based on [jekyll](https://jekyllrb.com),
with a modified version of the awesome [basically basic
theme](https://github.com/mmistakes/jekyll-theme-basically-basic) of [Micheal
Rose](https://mademistakes.com/). The hosting currently happens on
[codeberg](https://codeberg.org) a splendid alternative to hosting at `github`.

*Disclaimer*: Development of the  Belgian map was funded in 2022 by [Patrick
Breyer MEP][patrick-breyer] (Greens/EFA group, Pirate Party).

[patrick-breyer]: https://www.europarl.europa.eu/meps/en/197431/PATRICK_BREYER/home
