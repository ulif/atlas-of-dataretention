---
title: Europäische Union
layout: tag
taxonomy: eu
area: eu
lang: de
maps: [eu]
tag-name: eu
permalink: /a/eu/
---

Der Stand der Vorratsdatenspeicherung in der EU (insgesamt) ist ernst. Gründe
sind die kritische Überwachungssituation in den meisten EU-Staaten und Pläne
der EU-Kommission, die Vorratsdatenspeicherung aucg EU-weit verpflichtend zu
machen.


## Aktueller Zustand: Kritisch

Die Europäische Kommission plant eine zweite Vorratsdaten-Richtlinie und
scheint dabei von den meisten Mitgliedsländern im EU-Rat unterstützt zu werden.

Mitgliedsstaaten und die Europäische Kommission versuchen die Vorgaben des EUGH
und vieler nationaler Verfassungsgerichtshöfe zu umgehen oder auszuhöhlen.


{% include maps/map_eu1.html mapformat="big" %}

*Übersicht Vorratsdatenspeicherung in der EU.* Für weitere Infos, bitte auf einzelne Länder klicken.

Wir bewerten den Gesamtzustand der EU in Hinsicht auf Vorratsdatenspeicherungen
als kritisch, da die Mehrheit der EU-Einwohner.innen in Gebieten lebt, die
selbst kritisch zu bewertenden nationalen Vorratsdatenregelungen unterliegen.


## Geschichte der Vorratsdatenspeicherung in der EU

- 2014, Apr 08
: Der Europäische Gerichtshof **erklärt die Richtlinie für ungültig**, da anlasslose unbeschränkte Datensammlungen das in der EU Charta festgeschriebene Recht auf Privatheit verletzen.

- 2006, Mär 15
: Die **erste Richtlinie zur Vorratsdatenspeicherung** (Directive 2006/24/EC) wird verabschiedet. Sie verpflichtete u.a. zur Speicherung von IP-Adressen und Meta-Daten wie die Zeitstempel jeder Email, von Telefongesprächen oder SMS. Der behördliche Datenzugriff benötigte keine richterliche Ermächtigung.

[//]: ## Berichte aus der EU

