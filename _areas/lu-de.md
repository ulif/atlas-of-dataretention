---
title: Luxemburg
layout: tag
taxonomy: lu
area: lu
lang: de
maps: []
tag-name: lu
permalink: /a/lu/
---

Nach vielen Jahren des Wartens auf eine europaweite Regelung, stellte die
luxemburgische Regierung im Januar 2023 ein neues Gesetz zur
Vorratsdatenspeicherung vor. Trotz der zahlreichen zwischenzeitlich ergangenen
Urteile des EUGH zur Vorratsdatenspeicherung, war bis zu diesem Zeitpunkt immer
noch ein Gesetz von 2005 in Kraft.


## Aktueller Status: Kritisch

Während man argumentieren könnte, dass Luxemburg mit dem neuen Gesetz endlich
in den Kreis der grundrechtsfreundlichen Staaten zurückkehrt, sind die Details
jedoch besorgniserregend.

Tatsächlich versucht die neue Regelung die schon in Belgien eingeführte
Vorratsdatenspeicherung 2.0 einzuführen. Diese erlaubt gleich mehrere "Layer"
von Vorratsdatenspeicherungen. So müssen IP-Nummern auch ohne jeden Anlass
grundsätzlich von Kommunikationsdienstleistern gespeichert werden. In besonders
frequentierten Zonen müssen auch weitergehende Daten gespeichert und auf
Anfrage herausgegeben werden, etwas Standortdaten.

Im Endeffekt läuft dies auf eine anlasslose und unterschiedslose Überwachung
der Kommunikation des Großteils der gesetzestreuen Luxemburger Bevölkerung
hinaus.


## Geschichte der Vorratsdatenspeicherung in Luxemburg

- 2023, Jan 25
: Luxemburg veröffentlicht (nach 18 Jahren!) ein neues Gesetz zur
Vorratsdatenspeicherung. Das neue Gesetz sieht u.a. eine generelle und
anlasslose Vorratsdatenspeicherung der IP-Nummern, die Vorratsspeicherung von
Standortdaten in bestimmten Gebieten des Landes und einen
Quick-Freeze-Mechanismus zur Speicherung aller Kommunikationsdaten in
bestimmten Fällen, wie etwa bei vermissten Personen, vor.

- 2015
: Der luxemburgische Justizminister Felix Braz entwirft Änderungen am
bestehenden Gesetz. Der Entwurf wird aber nach mehreren EUGH-Entscheidungen im
Jahr 2015 wegen Inkompatibilität mit europäischem Recht aufgegeben.

- 2005, May 30
: Luxembourg introduces a general and indiscriminate data retention. The law
requires retention of traffic and location data for a time period of six
months.  Law enforcement bodies can request this data for purposes of the
investigation, detection and prosecution of criminal offences, which might be
punishable by a jail sentence of one year or more.

: Luxemburg führt eine allgemeine und unterschiedlose Vorratsdatenspeicherung
ein. Das Gesetz schreibt die Vorratsspeicherung von Verkehrs- und Standortdaten
für einen Zeitraum von sechs Monaten vor. Sicherheitsbehörden können diese
Daten zur Untersuchung, Ermittlung und Verfolgung von Straftaten mit einem
Strafrahmen von mindestens einem Jahr Gefängnis abrufen.


[//]: ## Berichte über Luxemburg
## Neues über Luxemburg

