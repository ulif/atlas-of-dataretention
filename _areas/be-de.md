---
title: Belgien
layout: tag
taxonomy: be
area: be
lang: de
maps: [be]
tag-name: be
permalink: /a/be/
---

Der aktuelle Stand der Vorratsdatenspeicherung ist kritisch. Grund sind Pläne der
Regierung, Vorgaben europäischer und belgischer Gerichtshöfe zu umgehen.

## Aktueller Stand: Kritisch

{% include maps/map_be1.html %}

<br>
Tatsächlich plant Belgien die **Vorratsdatenspeicherung 2.0** einzuführen,
eine neue Art von Vorratsdatenspeicherung, die auf dem Papier die Anforderungen
mehrerer Gerichtsentscheidungen möglicherweise erfüllt.

Letzlich vertärkt diese Art der Überwachung den Überwachungsdruck auf
gesetzestreue Bürger.innen in ganz Belgien und auf unbestimmte Zeit.


## Geschichte der Vorratsdatenspeicherung in Belgien

- 2022-07-07
: [Das belgische Parlament beschließt](https://www.brusselstimes.com/251735/moving-towards-mass-surveillance-belgian-approves-data-re) **zum dritten mal innerhalb von 10 Jahren** ein Gesetz zur Vorratsdatenspeicherung. Das neue Gesetz erweitert die rechtlichen Befugnisse der Sicherheitsbehörden im Vergleich mit den ersten Regelungen noch weiter und hat eine ganz neue Struktur (5 parallele Vorratsdatenspeicherungen). Es soll augenscheinlich als [Beispiel für andere europäische Staaten dienen](https://edri.org/our-work/belgium-wants-to-ban-signal-a-harbinger-of-european-policy-to-come/). Das dritte Gesetz verpflichtet Kommunikationsanbieter zur zusätzlichen Speicherung von Daten, die sie selbst eigentlich nicht speichern, z.B. Einzelverbindungsdaten trotz Flatrates.

- 2021-04-22
: Die **zweite Gerichtsentscheidung** des belgischen Verfassungsgerichtshofs zu Vorratsdatenspeicherungen [erklärt das zweite Vorratsdatenspeicherungsgesetz für nicht verfassungskonform](https://www.brusselstimes.com/belgium-news/166148/constitutional-court-throws-out-data-retention-law) nach Befragung des Europäischen Gerichtshofes.

- 2016
: Belgien beschließt das **zweite Vorratsdatenspeicherungsgesetz**, das alle Telekommunikationsanbieterinnen verpflichtet, Details jeder Transaktion für ein Jahr aufzubewahren, die in ihren Systemen stattfand. Alle Daten sollen unterschiedslos gespeichert werden.

- 2015
: Der belgische **Verfassungsgerichtshof versenkt das Gesetz** und folgt damit dem Europäischen Gerichtshof, der die Vorratsdatenspeicherungs-Richtlinie der EU im Fall **Digital Rights Ireland** annulliert hatte.

- 2013-07-30
: Das **erste Gesetz zur Vorratsdatenspeicherung** wird beschlossen. Es bildet die Umsetzung der EU-Richtlinie zur Vorratsdatenspeicherung und führt in Belgien die Vorratsdatenspeicherung der Daten von Telekommunikationsteilnehmenden sowie von Verkehrs- und Standortdaten elektronischer Kommunikation ein.

## Berichte zu Belgien

