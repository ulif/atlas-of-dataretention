---
title: Germany
layout: tag
taxonomy: de
area: de
lang: en
maps: []
tag-name: de
permalink: /a/de/
---

The ruling by the German Federal Administrative Court in August 2023 finally
invalidates the 2015 data retention regulation that had been in place until
then because it is not compatible with the European ePrivacy Directive. The
regulation, which had already been suspended since 2017, is thus no longer
applicable.


## Current state: Tense

The ruling is based primarily on the EU Court of Justice decision of September
2022.
                                                                                                         
Since the EUCJ -- in addition to clearly recommending the quick freeze
procedure -- grants some fundamental exceptions for certain forms of data
retention, there has since been a great deal of discussion, particularly
between the interior and and justice ministries of the federal and state
governments over the scope and nature of an upcoming regulation.

The governing coalition had mentioned data retention separately in its
coalition agreement:

  *"[...] we will develop the regulations on data retention in such a way that
data can be stored in a legally secure manner on an ad hoc basis and by
judicial order. "*

which was generally interpreted to mean that the coalition was fundamentally
opposed to data retention without any specific reason. Since the ruling,
however, the Conference of Ministers of the Interior, like the Federal Minister
of the Interior, Ms. Faeser, has been calling for the reintroduction of
no-cause IP data retention.

A draft submitted by Justice Minister Buschmann without an data retention, but
with implementation of the Quick Freeze procedure has so far been rejected by
Federal Minister of the Interior Faeser. The interdepartmental coordination is
apparently proving difficult. The situation therefore remains tense.


## History of data retention in Germany

- 2023, Aug 14
: The German Federal Administrative Court **confirms the incompatibility** of
the second German data retention regulation with EU law. The regulation is thus
**finally invalid**.

- 2022, Sep 20
: European Court of Justice declares **second German attempt** to introduce
data retention **incompatible with European law**.

- 2017, Jun
: The German Federal Network Agency suspends enforcement of data retention
following a ruling by the Münster Higher Administrative Court. This **de facto
suspends** data retention until a decision is made in the main proceedings, the
progress of which had been decided in Münster. The BGH ultimately referred the
basic issue of the proceedings to the ECJ for clarification, meaning that data
retention remained suspended until a decision was reached by the ECJ.

- 2015, Dec 17
: The **second data retention law** enters into force. It obliges
telecommunications providers to store: Location data of phone callers and, in
the case of mobile internet use, phone numbers, time and duration of all phone
calls and SMS, as well as assigned IPs.

- 2010, Mar 02
: The Federal Constitutional Court **declares the German regulations on VDS
invalid**.

- 2007, Nov 09
: The Bundestag passes the **first data retention law** with the votes of the
CDU/CSU and the SPD alone. Data that must be stored for up to seven months,
among other things: Phone numbers, IMEIs, IPs and location data of callers and
called parties, time stamps of communications as well as email addresses, IPs
and time stamps of sent emails. In addition, usernames, IPs and timestamps of
mailboxes as well as email addresses, IPs and timestamps of sent mails. In
addition, usernames, IPs and timestamps for mailbox retrievals. The inventory
data disclosure (name, address, date of birth, start of contract, user name,
passwords) is expanded to include all users of permanent connection IDs.

[//]: ## Berichte aus Deutschland
## News from Germany

