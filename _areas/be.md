---
title: Belgium
layout: tag
taxonomy: be
area: be
maps: [be]
tag-name: be
---

The current state of data retention is serious, due to plans of the government
to circumvent provisions made by European and Belgian courts.

## Current State: Critical

{% include maps/map_be1.html %}

<br>
In fact, Belgium plans to create **Data Retention 2.0**, a new kind of data
retention that on paper fulfills requirements of several court rulings.
Actually this kind of surveillance even intensifies surveillance pressure on
law-abiding citizens all over Belgium at all times.

## History of Data Retention in Belgium

- 2022, Jul 07
: [Belgian parliament approves **third data retention act**](https://www.brusselstimes.com/251735/moving-towards-mass-surveillance-belgian-approves-data-re) in less than 10 years. This one even extends the legal powers of law enforcements compared to the first acts and provides a new structire. Clearly it was made as [sample implemention for other European countries](https://edri.org/our-work/belgium-wants-to-ban-signal-a-harbinger-of-european-policy-to-come/).

- 2021, Apr 22
: The **second ruling** of Belgian Constitutional Court [declares second data retention law unconstitutional](https://www.brusselstimes.com/belgium-news/166148/constitutional-court-throws-out-data-retention-law) after asking the European Court of Justice.

- 2016
: Belgium approves the **second data retention act** which obliges all telecommunication companies to keep details of every transaction that takes place through their systems for one year. All data is to be stored without any discrimination.

- 2015
: The Belgian **Constitutional Court strucks down the act**, following the European Court of Justice’s annulment of the Data Retention Directive in the case of Digital Rights Ireland.

- 2013, Jul 30
: The **first data retention act** implemented the EU data retention directive. It introduced the retention of subscriber, traffic and localisation data of electronic communication in Belgium.

## Reports from Belgium

