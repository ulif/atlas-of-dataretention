---
title: European Union
layout: tag
taxonomy: eu
area: eu
maps: [eu]
tag-name: eu
permalink: /a/eu/
---

The current state of data retention in the EU (overall) is serious, due to the
bad surveillance situation in most member countries and plans of the
Commission.


## Current State: Critical

The European Commission has plans to pass a second data retention
directive and seems to be backed by most member states in the EU Council.

Member states and the European Commission try to circumvent provisions made by
European and plenty national constitutional courts.


{% include maps/map_eu1.html mapformat="big" %}

*Overview of Data Retention in the EU.* Click on single countries to learn more.


We consider the overall state of the EU critical, because the majority of
inhabitants of the EU lives in areas with critical national data retention
conditions.


## History of Data Retention in the EU

- 2014, Apr 08
: The Court of Justice of the European Union **declared the directive invalid**, because blanket data collection violated the right of privacy as enshrined in the EU Charter of Fundamental Rights.

- 2006, Mar 15
: The **first Data Retention Directive** (Directive 2006/24/EC) was passed. Under it IP-addresses and other details like time of use of every email, phone call, and text message sent or received had to be stored. Data access had not to be permitted by a court.


[//]: ## Reports from EU

