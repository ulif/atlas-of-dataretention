---
title: Luxembourg
layout: tag
taxonomy: lu
area: lu
lang: en
maps: []
tag-name: lu
permalink: /a/lu/
---

After many years waiting for a European regulation, the Luxembourg government
presented a data retention law in January 2023. Despite the numerous EU Court
of Justice's rulings on data retention, the law from 2005 was still in place. 


## Current state: Critical


While one could argue that the new law finally brings Luxembourg back into the
circle of fundamental rights-sensitive states, the details are worrying.

In fact, the new regulation attempts to introduce Data Retention 2.0, which has
already been introduced in Belgium. This allows several "layers" of data
retention at once. For example, IP numbers must generally be stored by
communications service providers even without any reason, while at the same
time in particularly frequented zones, additional data, such as location data,
must also be stored and released on request.

The bottom line is that this amounts to general and indiscriminate monitoring
of the communication of the majority of Luxembourg's law-abiding population.


## History of data retention in Luxembourg

- 2023, Jan 25
: Luxembourg publishes (after 18 years!) a new data retention bill. The new law
requires general data retention of all IP numbers used by people in Luxembourg,
a general retention of location data in certain areas of the country and a
Quick Freeze mechanism to store all communication data in certain cases like
missing persons.

- 2015
: The Luxembourg Minister of Justice, Felix Braz, sketches modifications to the
existing law. The draft is dropped after ECJ decisions in 2015, that render the
draft incompatible with European law.

- 2005, May 30
: Luxembourg introduces a general and indiscriminate data retention. The law
requires retention of traffic and location data for a time period of six
months.  Law enforcement bodies can request this data for purposes of the
investigation, detection and prosecution of criminal offences, which might be
punishable by a jail sentence of one year or more.

[//]: ## Reports from Luxembourg
## News from Luxembourg

