---
title: Deutschland
layout: tag
taxonomy: de
area: de
lang: de
maps: []
tag-name: de
permalink: /a/de/
---

Das Urteil des BVerwG vom August 2023 setzt die bis dahin bestehende Regelung
zur Vorratsdatenspeicherung von 2015 endgültig außer Kraft, weil sie mit nicht
mit der europäischen E-Privacy-Richtlinie vereinbar ist. Die bereits seit 2017
ausgesetzte Regelung ist damit endgültig nicht mehr anwendbar.


## Aktueller Zustand: Angespannt

Das Urteil stützt sich vor allem auf die EUGH-Entscheidung vom September 2022.

Da der EUGH -- neben einer deutlichen Empfehlung des Quick-Freeze-Verfahrens
-- einige grundsätzliche Ausnahmen für bestimmte Formen der
Vorratsdatenspeicherung einräumt, wird seitdem vor allem zwischen Innen- und
Justizministerien des Bundes und der Länder um Umfang und Art einer kommenden
Regelung gestritten.

Die Regierungskoalition hatte in ihrem Koalitionsvertrag die
Vorratsdatenspeicherung gesondert erwähnt:

  *„[...] werden wir die Regelungen zur
Vorratsdatenspeicherung so ausgestalten, dass Daten rechtssicher anlassbezogen
und durch richterlichen Beschluss gespeichert werden können.“*

was allgemein so interpretiert wurde, dass die Koaltion eine anlasslose
Vorratsdatenspeicherung grundsätzlich ablehne. Die Innenministerkonferenz
fordert aber seit dem Urteil, genau wie Bundesinnenministerin Faeser, trotzdem
die Wiedereinführung der anlasslosen IP-Vorratsdatenspeicherung.

Ein von Justizminister Buschmann vorgelegter Entwurf ohne anlasslose
Vorratsdatenspeicherung aber mit Implementierung des Quick-Freeze-Verfahrens
wird bislang von Bundesinnenministerin Faeser abgelehnt. Die Ressortabstimmung
gestaltet sich augenscheinlich schwierig. Die Lage ist daher weiter angespannt.


## Geschichte der Vorratsdatenspeicherung in Deutschland

- 2023, Aug 14
: Das **Bundesverwaltungsgericht** [bestätigt](https://www.bverwg.de/de/pm/2023/66) Unvereinbarkeit der deutschen Vorratsdatenspeicherung 2.0 mit EU-Recht. Die Regelung ist damit **endgültig ungültig**.

- 2022, Sep 20
: Der Europäische Gerichtshof erklärt den **zweiten deutschen Versuch** zur Einführung einer Vorratsdatenspeicherung für **unvereinbar mit europäischem Recht**.

- 2017, Jun
: Die Bundesnetzagentur setzt nach einem Urteil des Oberverwaltungsgerichts Münster die Durchsetzung der Vorratsdatenspeicherung aus. Damit **wird faktisch die Vorratsdatenspeicherung ausgesetzt** bis zu einer Entscheidung im Hauptsacheverfahren über dessen Fortgang in Münster entschieden worden war. Der BGH hat die Grundfrage des Verfahrens schließlich dem EUGH zur Klärung vorgelegt, so dass die Vorratsdatenspeicherung bis zu einer Entscheidung des EUGH ausgesetzt blieb.

- 2015, Dez 17
: Das **zweite Gesetz zur Vorratsdatenspeicherung** tritt in Kraft. Es verpflichtet Telekommunikationsanbieter.innen zur Speicherung von: Standortdaten von Telefonierenden und bei mobiler Internetnutzung, von Rufnummern, Zeit und Dauer aller Telefonate und SMS, sowie der zugewiesenen IPs.

- 2010, Mär 02
: Das Bundesverfassungsgericht **erklärt die deutschen Vorschriften zur VDS für ungültig**.

- 2007, Nov 09
: Der Bundestag verabschiedet allein mit den Stimmen der CDU/CSU und der SPD das **erste Gesetz zur Vorratsdatenspeicherung**. Gespeichert werden müssen für bis zu sieben Monate u.a.: Rufnummern, IMEIs, IPs und Standortdaten von Anrufenden und Angerufenen, Zeitstempel von Kommunikationen sowie Email-Adressen, IPs und Zeitstempel versandter Mails. Außerdem Usernamen, IP und Zeitstempel bei Mailbox- sowie Email-Adressen, IPs und Zeitstempel versandter Mails. Außerdem Usernamen, IP und Zeitstempel bei Mailbox-Abrufen. Die Bestandsdatenauskunft (Name, Anschrift, Geburtsdatum, Vertragsbeginn, Username, Passwörter) wird auf "alle Nutzer dauerhafter Anschlusskennungen" erweitert.

[//]: ## Meldungen aus Deutschland
## Meldungen aus Deutschland

