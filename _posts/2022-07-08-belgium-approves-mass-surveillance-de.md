---
layout: post
sub_title: "Belgien"
title:  "Parlament beschließt Vorratsdaten-Speicherung 2.0"
permalink: /2022/07/08/belgiums-approves-mass-surveillance.html
date:   2022-07-08 16:00:00 +0200
lang: "de"
tags: be
---


Gestern hat das belgische Parlament die [erstaunlichen Pläne der Regierung
gebilligt](https://www.brusselstimes.com/251735/moving-towards-mass-surveillance-belgian-approves-data-re),
einen dritten Versuch zur Errichtung einer Vorratsdatenspeicherung zu
unternehmen, der in Einklang mit den Grundrechten  bleibt.

Belgien wechselt daher in diesem Atlas wieder in den Status „kritisch“. Das
Land erscheint ab sofort wieder in rot auf der Titelseite.
