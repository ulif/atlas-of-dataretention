---
layout: post
sub_title: "Europäischer Gerichtshof"
title:  "Deutsche Vorratsdatenspeicherung 2.0 unvereinbar mit EU-Recht"
date:   2022-09-20 10:45:00 +0200
author: ulif
permalink: /2022/09/20/german-dataretention-2-0-canceled.html
# maps: be
tags: de
lang: de
---

Der Europäische Gerichtshof hat die deutsche Vorratsdatenspeicherung in der
Form des Gesetzes von 2015 für unvereinbar mit europäischem Recht erklärt.

Die Frage war dem EUGH vom Bundesverwaltungsgericht in Leipzig vorgelegt
worden, das über Anträge der Telekommunikationsprovider space.net und Deutsche
Telekom zu entscheiden hatte.

Die deutsche Vorratsdatenspeicherung ist aktuell geltendes Gesetz. Dessen
Durchsetzung war aber von der Bundesnetzagentur bereits im Jahr 2017 ausgesetzt
worden. Grund war ein Urteil des EUGH von 2016, in dem die Vereinbarkeit
nationaler allgemeiner und anlassloser Vorratsdatenspeicherungen mit Vorgaben
der ePrivacy-Verordnung und der Grundrechtecharta der EU deutlich verneint
wurde.

Die heutige Entscheidung ist bereits die fünfte des EUGH zu anlasslosen
Vorratsdatenspeicherungen in der EU und das Gericht blieb seiner Linie treu.
Dies betrifft allerdings auch die Bedingungen, unter denen die Speicherung von
Vorratsdaten sehr wohl mit EU-Recht vereinbar wäre (und damit die Tür offen
lässt für eine VDS 3.0).

Der EUGH findet in den folgenden Fällen eine **VDS okay**:

- Bei einer als real und aktuell oder vorhersehbar einzustufenden **ernsten
  Bedrohung für die nationale Sicherheit** des Staates. Auch in diesem Fall
  bedarf es eines Richtervorbehalts und die VDS muss auf einen absolut
  notwendigen und verlängerbaren Zeitraum begrenzt werden. Kommt dies alles
  zusammen können Verkehrs- und Standortdaten allgemein und unterschiedslos
  gespeichert werden.

- Zum Schutz der nationalen Sicherheit, zur Bekämpfung *schwerer* Kriminalität
  und zur Verhütung *schwerer* Bedrohungen öffentlicher Sicherheit
  [Hervorhebung im Orig.] darf eine **gezielte Vorratsdatenspeicherung** von
  Verkehrs- und Standortdaten durchgeführt werden. "Gezielt" bedeutet, dass
  objektve Kriterien festgelegt sein müssen, nach denen bestimmte
  Personengruppen oder auch geografische Gebiete bestimmt werden, in oder bei
  denen die VDS (Verkehrs- und Standortdaten) dann stattfinden darf. Das Urteil
  nennt als Beispiel für so ein Kriterium die Kriminalitätsrate in Gebieten.

- Die **allgemeine und unterschiedslose Vorratsdatenspeicherung der
  IP-Adressen** von Verbindungen ist für die Zwecke des vorgehenden Absatzes
  immer erlaubt.

- Die **Identitätsdaten** von Nutzenden elektronischer Kommunikationsmittel
  dürfen einer **allgemeinen und unterschiedslosen Vorratsdatenspeicherung**
  unterzogen werden, wenn das ganz allgemein der Kriminalitätsbekämpfung oder
  dem Schutz der öffentlichen oder nationalen Sicherheit dient. Dies betrifft
  also z.B. solche Daten, die wir beim Abschluss eines Telefonvertrags oder dem
  Kauf einer SIM-Karte angeben. Die Speicherung dieser Daten ist eine
  Vorratsdatenspeicherung und quasi immer erlaubt.

Der Gerichtshof weist in dem Urteil erneut darauf hin, dass insbesondere das
**Quick-Freeze-Verfahren**, bei dem Daten erst dann gespeichert ("eingefroren")
werden, wenn ein konkreter Verdacht gegen bestimmte Personen besteht, eine
rechtssichere Möglichkeit ist, Telekommunikationsdaten zu speichern.

Insgesamt bleiben also auch die Schlupflöcher offen, die der EUGH in den
vergangenen Urteilen geöffnet hatte. Dies auch dann, wenn er darauf hinweist,
dass beispielsweise eine gezielte Vorratsdatenspeicherung nicht durch die
Nutzung geschickt ausgedachter Kategorien zu einer allgemeinen und
unterschiedslosen Vorratsdatenspeicherung werden darf. Letzteren Fall könnte in
Belgien angenommen werden, wo die Schwellwerte für „verbrechensbelastete
Gebiete“ so niedrig angesetzt wurden, dass im Endeffekt das ganze Land als
„verbrechensbelastet“ und damit vorratsdatensspeicherungswürdig eingestuft
wurde.

In Deutschland ist es wahrscheinlicher, dass die reine
IP-Vorratsdatenspeicherung zur deutschen VDS 3.0 werden könnte, denn sie ist
technisch und rechtlich relativ einfach zu implementieren und gestattet Zugriff
auf sehr persönliche Verhältnisse. Der Gerichtshof weist selbst darauf hin,
dass mit IP-Nummern leicht Profile gebildet werden können.

Die Entscheidung des EUGH ist **keine Folge der vielen Verfassungsbeschwerden**
gegen die Vorratsdatenspeicherung. Vielmehr beziehen sich die betroffenen
Unternehmen SpaceNet und Deutsche Telekom auf Regelungen der
**e-Privacy-Richtlinie**, also EU-Recht, die zusammen mit der Grundrechtecharta
der EU zu einem Verbot von Vorratsdatenspeicherungen führe.

Das Bundesverfassungsgericht hatte auch in seiner Entscheidung gegen die
deutsche Vorratsdatenspeicherung 1.0 von 2010 **an der eigentlichen Speicherung
nur wenig auszusetzen** gehabt. Es bemängelte lediglich den nur eingeschränkt
gesicherten Abruf dieser gespeicherten Daten durch Behörden.

Es ist daher nicht unwahrscheinlich, dass die Verfassungsbeschwerden in
Deutschland gescheitert wären oder vielleicht noch scheitern werden, sofern das
BVerfG sich mit ihnen im Lichte der EUGH-Entscheidung überhaupt noch
beschäftigen wird.

Dieses kleine Detail ist insofern wichtig, da der **Schutz vor ausufernder
Massdatenspeicherung von Vorratsdaten in Deutschland also allein auf EU-Recht
basiert** und noch konkreter auf der e-Privacy-Richtlinie. Diese wird aber
momentan zwischen EU-Kommission, Rat und Parlament neu ausverhandelt.

Und da den überwachungsfreudigen Mitgliedsstaaten dieses Detail nicht verborgen
geblieben ist, haben sie auch schon Vorschläge gemacht, die
Vorratsdatenspeicherungen künftig grundsätztlich als Regelungs-Angelegenheit
der nationalen Sicherheit zu werten. Der pikant-bittere Nebeneffekt: dann
wäre die Bewertung von Vorratsdatenspeicherungen keine EU-Angelegenheit mehr -
der EUGH hätte keine Befugnis zur Regelung mehr. **Alle EUGH-Urteile und ihre
Grundrechtserwägungen wären obsolet**. In Deutschland würde das
Bundesverfassungsgericht zur letzten Instanz. Und dort wäre nicht viel Gutes zu
erwarten.
