---
layout: post
sub_title: "Belgien"
title:  "Fümpf Vorratsdatenspeicherungen auf einmal"
date:   2022-06-18 15:00:00 +0200
author: ulif
permalink: /2022/06/18/belgiums-next-gen-dataretention.html
maps: be
tags: be
lang: de
---

Belgien versucht, die Urteile belgischer und europäischer Gerichte zu umgehen -
durch die Einführung von **fümpf Vorratsdatenspeicherungen auf einmal**.

Dass die anlasslose und unterschiedslose Speicherung der Daten aller Menschen
in EU-Staaten nicht die Gegenliebe der Gerichte findet, ist auch Regierungen
nicht verborgen geblieben. Ein Paradebeispiel ist Belgien. Bereits zwei mal hat
dort der Verfassungsgerichtshof existierende Gesetze zur
Vorratsdatenspeicherung beerdigt. Doch der Zombie findet keine Ruhe.

Die belgische grün-sozialdemokratisch-konservative Regierung startet nun eine
Wiederbelebung, die es in sich hat: gleich fünf Vorratsdatenspeicherungen
sollen eine Umgehungsstraße um die nörgeligen Vorgaben höchster belgischer und
europäischer Gerichte bilden. Die Hoffnung dahinter: zusammengesetzt ergibt
sich aus dem Datengeflecht jenes Meer von Daten, dessen Erhebung die Gerichte
in Einzelregelungen immer abgelehnt haben. Das brisante Vorhaben wird von der
belgischen Regierung explizit als Modell auch für andere EU-Staaten angesehen:
Seht her! So lassen sich die Vorgaben der Gerichte umgehen!

Kurzgefasst sehen die geplanten fünf Layer der Überwachung so aus:

- Layer 1: IP-Speicherung
: Da die Gerichte IP-Adressen teils als nicht personenbeziehbare Daten gewertet haben, machen sich belgische (und auch deutsche) Ermittler einen schlanken Fuß und setzen die Grundrechtskonformität einer verpflichtenden, allgemeinen und anlasslosen Vorratsdatenspeicherung einfach als gesetzt. **IP-Adressen müssen also nach der Regelung immer und überall in Belgien gespeichert werden.**

- Layer 2: Kriminalität der Landesteile
: Die Gerichte haben anlasslose Speicherungen abgelehnt, aber eingeräumt, dass anlassbezogene Speicherungen, die nicht alle betreffen, erlaubt sein könnten. Diese Lücke möchte die belgische Regierung nutzen, indem sie die Vorratsdatenspeicherung für jeden von nur 16 Landesteilen aktiviert, sobald die Kriminalität einen bestimmten Schwellwert überschreitet. Die Landesteile sind sehr groß, die Schwellwerte sehr klein und ziemlich statisch im Gesetzentwurf festgelegt. Im Endergebnis führt die Regelung zu einer **ganzjährigen Vorratsdatenspeicherung auf der gesamten Landesfläche**.

- Layer 3: Nationales Terror-Bedrohungsniveau
: Bei Vorliegen einer konkreten Bedrohung haben die Gerichte die Erhebung von Vorratsdaten ebenfalls gestattet. Belgien führt eine vierstufige „Terror-Bedrohungsskala“. Steigt sie auf drei, soll Speichern künftig zur Pflicht werden. Im gesamten Land. Nach den Terror-Attacken in Paris 2015 stand die belgische Terror-Bedrohunsskala **über zwei Jahre auf drei oder höher**. So eine Bedrohung scheint sich also hinziehen zu können.

- Layer 4: Gefährdete Orte
: Relativ viele Regelungen zielen auf, wie sag ichs? gefahrgeneigte Orte und Zonen ab. Das sind regelmäßig Orte, an denen sich besonders viele Menschen tummeln und in, an oder auf denen sich Wesen gewöhnlich sehr häufig aufhalten: Bahnhöfe, Häfen, Autobahnen, Sportstadien, you name it. All diese Orte (und noch viel mehr) fallen unter ein permanentes Überwachungsregime durch verpflichtende Vorratsdatenspeicherung.

- Layer 5: Internationale Organisationen
: Alles was besonders geschützt werden muss, ist nach der Logik der belgischen Regierung besonders bedroht. Das betrifft auch internationale Einrichtungen wie das EU-Parlament oder Einrichtungen der Vereinten Nationen. An solchen Orten, an denen eigentlich vertrauliche Kommunikation besonders geschützt werden sollte, wird, wenn das Gesetz Wirklichkeit wird, künftig rund um die Uhr mitgeschnitten, wer wann mit wem wie lange telefoniert.


Wir haben die Pläne genauer untersucht und sind zu dem Ergebnis gekommen, dass
sogar einzelne, isolierte der geplanten Vorratsdatenspeicherungen zu einer
flächendeckenden Vorratsdatenspeicherung mit mindestens neun Monaten
Speicherfrist führt. Hier etwa der Layer 2:

{% include maps/map_be1.html %}
*Layer 2: Kriminelle Landesteile*

Die Karte zeigt, dass nur ein sehr kleiner Landesteil, der an der östlichen Landesgrenze gelegegene Distrikt Eupen, unter der Höchstspeicherdauer von einem Jahr bliebe. Aber selbst dort würde 9 Monate flächendeckend gespeichert werden.
