---
layout: post
sub_title: "Bundesverfassungsgericht"
title:  "Drei Klagen gegen Vorratsdatenspeicherung nicht angenommen"
date:   2023-03-30 10:45:00 +0200
author: ulif
permalink: /2023/03/30/german-const-court-dissmisses-complaints.html
# maps: be
tags: de
lang: de
---

Das Bundesverfassungsgericht hat drei Klagen gegen die deutsche
Vorratsdatenspeicherung [nicht zur Entscheidung
angenommen](https://www.bundesverfassungsgericht.de/SharedDocs/Pressemitteilungen/DE/2023/bvg23-037.html). Drei weitere Klagen
stehen nach Angaben der dpa noch aus.

Die Nicht-Annahme wurde wesentlich dadurch begründet, dass die Klagenden nach
der Entscheidung des EUGH vom 20. September 2022 nicht dargelegt hätten,
inwiefern sie weiterhin Rechtsschutz benötigten. Im genannten Urteil des EUGH
hatte dieser entschieden, dass u.a. die aktuelle deutsche Regelung zur
Vorratsdatenspeicherung nicht mit EU-Recht und der Grundrechte-Charta der
Europäischen Union vereinbar sei. Der Gerichtshof hatte dabei auch die
Gesetzespassagen benannt, die die Klagenden in ihrer Klage vor dem
Bundesverfassungsgericht gerügt hatten.

Das Verfassungsgericht betonte, dass die Entscheidung des EUGH bedeute, dass
die deutsche Regelung mit EU-Recht nicht vereinbar sei.

Einige der Klagenden [äußerten sich
erfreut](https://digitalcourage.de/blog/2023/vorratsdatenspeicherung-schlussstrich)
über die Entscheidung des deutschen Verfassungsgerichts.

Nicht angenommen wurden wenigstens die Klagen des Vereins Digitalcourage, sowie
des MEP Dr. Patrick Breyer. Nach [Angaben von Dr. Breyer
https://www.patrick-breyer.de/internet-vorratsdatenspeicherung-zerstoerung-der-anonymen-internetnutzung-waere-ein-dammbruch-den-es-zu-verhindern-gilt/]
stehen Entscheidungen des Bundesverfassungsgerichts für Klagen von Vertetern
der Grünen, der FDP, sowie 22 Beschwerdeführer.innen noch aus.

