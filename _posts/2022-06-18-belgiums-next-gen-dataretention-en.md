---
layout: post
sub_title: "Belgium"
title:  "Five Data Retentions at Once"
date:   2022-06-18 15:00:00 +0200
author: ulif
permalink: /2022/06/18/belgiums-next-gen-dataretention.html
maps: be
tags: be
lang: en
---

Belgium is trying to circumvent the rulings of Belgian and European courts - by
introducing **five data retentions at once**.

The fact that the general and indiscriminate storage of the data of all
people in EU countries does not meet with the approval of the courts has not
gone unnoticed by governments. Belgium is a prime example. The Constitutional
Court there has already buried existing data retention laws twice. But the
zombie is not at rest.

The Belgian green-social-democratic-conservative government is now launching a
revival that has it all: no fewer than five data retention laws are to form a
bypass around the nagging requirements of the highest Belgian and European
courts. The hope behind this is that the data network will produce the lakes of
data that the courts have always refused to collect in case of single data
retention laws. The explosive project is explicitly regarded by the Belgian
government as a model for other EU states as well: Behold! This is how to
circumvent the requirements of the courts!

In a nutshell, the planned five layers of surveillance look like this:

- Layer 1: IP storage
: Since the courts have partly considered IP addresses to be non-personal data, Belgian law-makers are taking the easy way out and simply assume that mandatory, general data retention without any reason complies with fundamental rights. IP addresses must therefore always be stored everywhere in Belgium under the regulation.

- Layer 2: Crime in the country
: The courts have rejected no-cause retention, but have acknowledged that cause-related retention that does not affect everyone could be allowed. The Belgian government wants to exploit this loophole by activating data retention for each of only 16 parts of the country once crime exceeds a certain threshold. The country parts are very large, the thresholds very small, and fairly static in the bill. The end result is that the scheme leads to year-round data retention across the entire country.

- Layer 3: National terror threat level.
: Where there is a specific threat, the courts have also permitted the collection of retained data. Belgium maintains a four-level "terror threat scale." If it rises to three, retention is to become mandatory in the future. Throughout the country. After the terrorist attacks in Paris in 2015, the Belgian terror threat scale was at three or higher for two years. So a threat like that seems to drag on.

- Layer 4: Places at risk
: Relatively many regulations target, how they are called, danger-prone places and zones. These are regularly places where a particularly large number of people congregate and where beings usually spend a lot of time in, on, or around: Train stations, ports, highways, sports stadiums, you name it. All these places (and many more) fall under a permanent surveillance regime through mandatory data retention.

- Layer 5: International Organizations
: According to the Belgian government's logic, everything that needs special protection is particularly threatened. This also applies to international institutions such as the EU Parliament or United Nations agencies. In such places, where confidential communications should actually be specially protected, if the law becomes reality, in the future it will be possible to record around the clock who is talking to whom on the phone, when, and for how long.

We have examined the plans in more detail and have come to the conclusion that even individual, isolated ones of the planned data retention measures will lead to blanket data retention with a storage period of at least nine months. Here, for example, is layer 2:

{% include maps/map_be1.html %}
Layer 2: Criminal parts of the country

The map shows that only a very small part of the country, the district of Eupen, located on the eastern border of the country, would remain below the maximum storage period of one year. But even there, 9 months of storage would cover the whole country.


