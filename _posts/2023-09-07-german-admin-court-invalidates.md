---
layout: post
sub_title: "German Federal Administrative Court"
title:  "Second German Data Retention Finally Buried"
date:   2023-09-07
author: ulif
permalink: /2023/09/07/german-admin-court-invlidates-dr.html
# maps: be
tags: de
lang: en
---

On August 18, 2023, the German Federal Administrative Court
[declared](https://www.bverwg.de/de/pm/2023/66) the retention of traffic data,
as legally regulated by the German government in the Telecommunications Act in
2015, to be **incompatible with European law in its entirety**.

This is probably the last step in two court cases that had already been brought
in 2016, by Deutsche Telekom and the company Space.Net.

The rulings are essentially based on the [decision of the EU Court of
Justice]({% post_url 2022-09-20-german-dataretention-2-0-canceled-de %}) in
September 2022, in which it ruled in response to a request for a preliminary
ruling from the BVerwG: the German regulation on DR in the Telecommunications
Act is not compatible with European law.

The court rejects the obligation to retain data not only for the old
Telecommunications Act of 2015 but also for the current version.

It explicitly includes in the prohibition the collection of IP numbers, for
which the EU Court of Justice had formulated exceptions to the ban on data
retention under very narrow conditions.

This means that the second German data retention law of 2015 has finally been
buried.
