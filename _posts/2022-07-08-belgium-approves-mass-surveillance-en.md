---
layout: post
sub_title: "Belgium"
title:  "Parliament Approves Data Retention 2.0"
date:   2022-07-08 16:00:00 +0200
permalink: /2022/07/08/belgiums-approves-mass-surveillance.html
lang: "en"
tags: be
---

Yesterday, the [Belgian parliament approved](https://www.brusselstimes.com/251735/moving-towards-mass-surveillance-belgian-approves-data-re) the stupendous plans of the government for the third try to create a data retention in line with fundamental rights.

Belgium therefore changes status to critical in this atlas. It will again
appear in red on the front page.
