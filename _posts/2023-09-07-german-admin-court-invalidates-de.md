---
layout: post
sub_title: "Bundesverwaltungsgericht"
title:  "Zweite deutsche Vorratsdatenspeicherung endgültig beerdigt"
date:   2023-09-07
author: ulif
permalink: /2023/09/07/german-admin-court-invlidates-dr.html
# maps: be
tags: de
lang: de
---

Das Bundesverwaltungsgericht hat bereits am 18. August 2023 die
Vorratsdatenspeicherung von Verkehrsdaten, wie sie im Jahr 2015 von der
Bundesregierung im Telekommunikationsgesetz gesetzlich geregelt wurde **in
vollem Umfang als unvereinbar mit europäischem Recht**
[erklärt](https://www.bverwg.de/de/pm/2023/66).

Dies ist der wahrscheinlich letzte Schritt in zwei Gerichtsverfahren, die
bereits 2016 angestrengt worden waren, von der Deutschen Telekom und der Firma
Space.Net.

Die Urteile basieren wesentlich auf der [Entscheidung des EUGH]({% post_url
2022-09-20-german-dataretention-2-0-canceled-de %}) vom September 2022, in der
dieser auf ein Vorabentscheidungsersuchen des BVerwG hin festellte: die
deutsche Regelung zur VDS im Telelkommunikationsgesetz ist nicht vereinbar mit
europäischem Recht.

Das Gericht weist die Verpflichtung zur Vorratsdatenspeicherung nicht nur für
das alte Telekommunikationsgesetz von 2015 zurück sondern auch für die aktuelle
Fassung.

Es schließt in das Verbot explizit auch die Erfassung von IP-Nummern ein, für
die der EUGH unter sehr engen Voraussetzungen Ausnahmen vom Verbot der
Vorratsdatenspeicherung formuliert hatte.

Damit ist die zweite deutsche Vorratsdatenspeicherung von 2015 wohl endgültig
beerdigt worden.
