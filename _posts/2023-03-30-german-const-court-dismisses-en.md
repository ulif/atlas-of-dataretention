---
layout: post
sub_title: "German Constitutional Court"
title:  "Lawsuits against Data Retention Dismissed"
date:   2023-03-30 10:45:00 +0200
author: ulif
permalink: /2023/03/30/german-const-court-dissmisses-complaints.html
# maps: be
tags: en
lang: en
---

The Federal Constitutional Court of Germany has
[dismissed](https://www.bundesverfassungsgericht.de/SharedDocs/Pressemitteilungen/DE/2023/bvg23-037.html)
three lawsuits against the German data retention. Three further lawsuits are
still pending, according to dpa.

The main reason given for the refusal was that the plaintiffs had not
explained the extent to which they still needed legal protection following the
decision of the European Court of Justice of September 20, 2022. In the
aforementioned judgment, the Court had ruled that, among other things, the
current German regulation on data retention was not compatible with EU law and
the Charter of Fundamental Rights of the European Union. The Court also named
the legal passages that the plaintiffs had criticized in their lawsuit before
the Federal Constitutional Court.

The Constitutional Court emphasized that the ECJ's decision meant that the
German regulation was not compatible with EU law.

Some of the plaintiffs [expressed their
delight](https://digitalcourage.de/blog/2023/vorratsdatenspeicherung-schlussstrich)
at the decision of the German Constitutional Court.

Dismissed were at least the complaints of the association Digitalcourage, as
well as MEP Dr. Patrick Breyer. According to [information from Dr.
Breyer](https://www.patrick-breyer.de/internet-vorratsdatenspeicherung-zerstoerung-der-anonymen-internetnutzung-waere-ein-dammbruch-den-es-zu-verhindern-gilt/)
decisions of the Federal Constitutional Court are pending for lawsuits of
representatives of the of the German Green Party, the German liberal party FDP,
and 22 more complainants are still
pending.

