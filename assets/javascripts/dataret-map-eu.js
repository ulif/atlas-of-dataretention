document.addEventListener("DOMContentLoaded", function(event) {
    var lang = document.querySelectorAll("[http-equiv='Content-Language']")[0].content;
    var baseurl = document.querySelector("#home-link").href;
    var mymap = L.map('mapidEU').setView([48.75, 6.5], 5);
    L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, geodata &copy; 2020 <a href="https://ec.europa.eu/eurostat/de/web/gisco/geodata/reference-data/administrative-units-statistical-units/countries#countries20">EuroGeographics for the administrative boundaries</a>'
    }).addTo(mymap);
    // at this point, `crimedata` must be defined (read from data/crimedata.js)
    var data = dr_data_eu[0];
    var geojson;

    function highlightFeature(e) {
        var layer = e.target;

        layer.setStyle({
            weight: 2,
            color: '#ff9',
            dashArray: '',
            fillOpacity: 0.7
        });

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }

        //mapinfo.update(layer.feature.properties);
    }

    function resetHighlight(e) {
        geojson.resetStyle(e.target);
        //mapinfo.update();
    }

    function zoomToFeature(e) {
        mymap.fitBounds(e.target.getBounds());
    }

    function onEachFeature(feature, layer) {
        var descr;
        var country_name;
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight,
            click: zoomToFeature
        });
        switch (lang) {
            case 'de':
                country_name = feature.properties.NAME_DE;
                descr = feature.properties.DESCR_DE; break;
            case 'en':
            default:
                country_name = feature.properties.NAME_ENGL;
                descr = feature.properties.DESCR;
        }
        descr = descr.replace("[BASEURL]", baseurl);
        layer.bindPopup( "<strong>" + country_name + "</strong>" +
            "<br><br>" + descr
        );
    }

    function style(feature) {
        var fillColor = '#dddd00',
            dr_status = feature.properties.STATUS;
        switch (dr_status) {
            case '0':
                fillColor = '#4f4'; break;
            case '1':
                fillColor = '#9f0'; break;
            case '2':
                fillColor = '#f90'; break;
            case '3':
                fillColor = '#f00'; break;
            default:
                fillColor = '#44444488';
        }
        return {
            fillColor: fillColor,
            weight: 1,
            color: '#88888800',
            fillOpacity: 0.5,
            opacity: 0.99,
            dashArray: '3',
        };
    }

    // add GeoJSON layer to the map once the file is loaded
    geojson = L.geoJson(data, {
        style: style,
        onEachFeature: onEachFeature
    }).addTo(mymap);


	// control that shows state info on hover
	var info = L.control();

	info.onAdd = function (map) {
		this._div = L.DomUtil.create('div', 'mapinfo');
		this.update();
		return this._div;
	};

	info.update = function (props) {
		this._div.innerHTML = '<h1>Data Retention in the EU</h1>' +  (props ?
			'<b>' + props.name + '</b><br />' +
            ' people / mi<sup>2</sup>' :
            'Average number of criminal offences against article 90ter <br> §§ 2 to 4 of Belgian criminal code of criminal procedure<br> per year from 2018 to 2020 for all police zones in Belgium.<br><br>This is layer 2 of the planned 5 layer data retention in Belgium.<br>' +
            'It contains juridicial districts, where from 2018 to 2020 more than<br>' +
            '3 offences per 1.000 inhabitants and per year happend.<br><br>' +
            '<b>This threshold is fixed and set by the government.<br><br>');
	};
	// info.addTo(mymap);


    // the legend in the lower right
    var lang = document.querySelectorAll("[http-equiv='Content-Language']")[0].content;
	var legend = L.control({position: 'bottomright'});
	legend.onAdd = function (map) {
		var div = L.DomUtil.create('div', 'mapinfo maplegend eu1');
        var title = "Status of<br>Data Retention";
        var t1 = "Never had DR", t2 = "DR stopped by Courts",
            t3 = "DR temporary inactive", t4 = "Active Data Retention";
        switch ( lang ) {
            case "de":
                title = "Status der<br>Vorratsdatenspeicherung";
                t1 = "Noch nie VDS"; t2 = "VDS abgeschafft";
                t3 = "VDS zeitweise außer Kraft"; t4 = "VDS aktiv";
                break;
        }
        div.innerHTML += '<center>' + title + '</center><br>'
        div.innerHTML += '<b class="val0">&nbsp;0</b> ' + t1 + '<br>' +
                         '<b class="val1">&nbsp;1</b> ' + t2 + '<br>' +
                         '<b class="val2">&nbsp;2</b> ' + t3 + '<br>' +
                         '<b class="val3">&nbsp;3</b> ' + t4 + '<br>';
        return div;
	};

	legend.addTo(mymap);
});
