---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
alt_title: Atlas der Vorratsdaten-speicherungen in der EU<b class="blink">.</b>
sub_title: Das Tracking unserer Daten in der EU sichtbar machen
date: 2023-09-08
maps: [eu, be, de]
lang: de
permalink: /
introduction: Unsere Daten werden gespeichert. Wen wir anrufen, wann wir chatten und wo wir da gerade waren. All watched over by machines of loving grace<b class="blink">.</b>
entries_layout: grid
---

{% include article-meta.html %}

# Aktueller Stand

Unzählige Urteile von Verfassungsgerichtshöfen überall in der EU haben
entschieden, dass eine allgemeine und anlasslose Vorratsdatenspeicherung ganzer
Staatsbevölkerungen gegen die Grundrechte verstößt. Trotzdem versuchen die
meisten EU-Staaten immer noch diese durchzusetzen. Wieder und immer wieder. Im
Endergebnis sind Vorratsdatenspeicherungen in den meisten europäischen Ländern
verpflichtende tägliche Realität.

Klicken sie auf ein Land, um mehr über dessen Vorratsdatensituation zu erfahren.

{% include maps/map_eu1.html mapformat="slim" %}

*Überblick – Vorratsdatenspeicherungen in der EU*

{% capture myvar %}{% link _areas/eu-de.md %}{% endcapture %}
{% include more-link.html href=myvar %}

