# The Atlas of Data Retention in the EU

This is just a little jekyll site to present the maps created in this project
in a more accessible way.


## Build / Install

Install `jekyll` and the [`basically basic`
theme](https://mmistakes.github.io/jekyll-theme-basically-basic/).

We also make use of the `jekyll-polyglot` gem to allow translated pages.

Building the static site boils down to:

    $ jekyll b

The final site is in `_site/`.

With

    $ jekyll s

you can watch the site on

  https://localhost:4000/




