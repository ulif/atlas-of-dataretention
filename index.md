---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
alt_title: Atlas of Data Retention in the EU<b class="blink">.</b>
sub_title: See what data is tracked everywhere in the EU
date: 2023-09-08
maps: [eu, be, de]
lang: en
permalink: /
introduction: Your data is stored. Whom you call, what messages you sent. All watched over by machines of loving grace<b class="blink">.</b>
entries_layout: grid
---

{% include article-meta.html %}

# The Current State

Although plenty court rulings of constitutional courts all over the EU decided,
that a general data retention of whole populations in the EU is not legal, most
member states are still trying to push for it. Again and again. As a result, in
most european countries data retention is enforced.

Click on any member state to learn more about its data retention situation.

{% include maps/map_eu1.html mapformat="slim" %}

*Overview – Data Retention in the EU*

{% capture myvar %}{% link _areas/eu.md %}{% endcapture %}
{% include more-link.html href=myvar %}

