#!/usr/bin/python3
##
## prep-eu.py
##
## Prepare EU-related maps. Merge data.
## Output is a JavaScript definition of
## the geojson data.
##
## output var is named `dr_data_eu`.
## Relevant feature attributes:
##  `DESCR` -> description text
##  `STATUS` -> data retention status
##    (0 = no DR, 1 = no DR, but had some
##     2 = currently disabled, 3 = DR active)
##
## Description will contain links to available
## country subpages.
##
## This script requires the `pyyaml` package
## Install with:
##
##     pip install pyyaml
##
## (c) 2022 ulif <uli@gnufix.de>
##
## License: gpl-3.0-or-later
##
##############################################
import csv
import json
import os
import yaml


GEODATA_JSON = "eu-countries.geojson"
DRDATA_CSV = "dr-in-eu.csv"
OUTFILE = "dataretention_eu.js"


PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))


def link_to_country_page(cntr_id, data, lang=""):
    """Return an absolute link to respective country page.

    `data` is the dict of a CSV row. If no such country page exists, return
    empty stgring.
    """
    c_id = cntr_id.lower()
    c_path = "%s.md" % (c_id)
    more = "More"
    if lang == "de":
        more = "mehr"
    if os.path.exists(os.path.abspath(os.path.join(PROJECT_ROOT, "_areas", c_path))):
        # string "[BASEURL]" has to be replaced by JavaScript
        # we cannot tell here, what the served root URL is.
        link = "<a href='[BASEURL]a/%s'>%s...</a>" % (c_id, more)
        return "<br><div class='map-internal-link'>%s</div>" % (link, )
    return ""


def merge_data(geodata_json, drdata_csv):
    with open(drdata_csv) as csvfile:
        data = dict(
            [(row["CNTR_ID"], row) for row in csv.DictReader(csvfile, delimiter=",")]
        )
    geodata = json.load(open(geodata_json))
    for feature in geodata["features"]:
        cntr_id = feature["properties"]["CNTR_ID"]
        if cntr_id not in data.keys():
            print("WARNING! No data for: %s" % cntr_id)
            continue
        descr    = data[cntr_id]["DESCR"]    + link_to_country_page(cntr_id, data)
        descr_de = data[cntr_id]["DESCR_DE"] + link_to_country_page(cntr_id, data, lang="de")
        feature["properties"]["DESCR"] = descr
        feature["properties"]["DESCR_DE"] = descr_de
        feature["properties"]["NAME_DE"] = data[cntr_id]["NAME_DE"]
        feature["properties"]["STATUS"] = data[cntr_id]["STATUS"]
    return geodata


def geojson2js(data, outfile):
    with open(outfile, "w") as fd:
        fd.write("var dr_data_eu = [%s];" % json.dumps(data))
    print("Result written to ", outfile)


geojson2js(merge_data(GEODATA_JSON, DRDATA_CSV), OUTFILE)
