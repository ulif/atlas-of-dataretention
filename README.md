# atlas-of-dataretention

The atlas of data retention in the EU. See what data is tracked everywhere in the EU.

The rendered version is currently visible at codeberg:

  https://ulif.codeberg.page/atlas-of-dataretention/

The *Atlas of Data Retention in the EU* is a sparetime project of some data
protection and privacy activists, mainly from Germany. It is a static website
based on `jekyll` and a modified version of the `basically basic` theme for
`jekyll`.

It was started by [ulif](https://codeberg.org/ulif) to tackle the lack of
up-to-date infos about data retention all over the EU. We try hard to update
and examine our data as quick as possible but we cannot guarantee to be
accurate in all details all of the time.

We started with base data from a [2019 map of
netzpolitik.org](https://netzpolitik.org/2019/vorratsdatenspeicherung-in-europa-wo-sie-in-kraft-ist-und-was-die-eu-plant/).

Our goal is not only to provide reliable details about DR but also to make this
complex type of data easer to grasp for mere mortals. To do that, we are
interested in maps, indexes, and other kinds of reports that give a good
overview over complex topics.

We know, that the data provided is not complete nor in all aspects still true
(laws changed a lot since 2019), therefore we are happy to receive your issues
and messages about out-of-data infos, etc.
